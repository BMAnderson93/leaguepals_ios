//
//  SummonerProfile.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/14/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation

class SummonerProfile {
    
    var appID : String
    var summonerName : String
    var following : [String]
    var summonerID : Int
    var summonerIconID : Int
    var summonerLevel : Int
    var ratings : [Int]
    var hasPost : Bool
    var hasJoinedGroup : Bool
    var isBanned : Bool
    
    var isAdmin: Bool
    
    init(appID: String, summonerName : String, summonerID : Int,summonerIconId : Int, summonerLevel : Int, hasPost : Bool,  isAdmin: Bool = false, hasJoinedGroup: Bool, isBanned: Bool, following : [String], ratings : [Int]) {
        self.appID = appID
        self.summonerName = summonerName
        self.summonerID = summonerID
        self.summonerIconID = summonerIconId
        self.summonerLevel = summonerLevel
        self.hasPost = hasPost
        
        self.isAdmin = isAdmin
        self.hasJoinedGroup = hasJoinedGroup
        self.isBanned = isBanned
        self.ratings  = ratings
        self.following = following
    }
}
