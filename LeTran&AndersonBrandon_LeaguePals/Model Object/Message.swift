//
//  Message.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/16/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class Message {
    var message: String
    var timestamp: Int
    var recipientUID: String
    var recipientName: String
    var senderUID: String
    var senderName: String
    
    init(_message: String, _timestamp: Int, _recipientUID: String, _recipientName: String, _senderUID: String, _senderName: String) {
        self.message = _message
        self.timestamp = _timestamp
        self.recipientUID = _recipientUID
        self.recipientName = _recipientName
        self.senderUID = _senderUID
        self.senderName = _senderName
    }
    
    func chatPartnerUID() -> String? {
        return senderUID == Auth.auth().currentUser?.uid ? recipientUID : senderUID
    }
}
