//
//  Post.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/15/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation

class Post {
    var roles: [String]
    var poster: String
    var gameMode: String
    var rankRequirement: String
    var mic: String
    var posterUID: String
    var time: String
   
    
    init(_roles: [String], _poster: String, _gameMode: String, _rankRequirement: String, _mic: String, _posterUID: String, _time: String) {
        self.roles = _roles
        self.poster = _poster
        self.gameMode = _gameMode
        self.rankRequirement = _rankRequirement
        self.mic = _mic
        self.posterUID = _posterUID
        self.time = _time
        
    }
}
