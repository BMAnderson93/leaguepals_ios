//
//  YoutubePost.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/25/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation

class YoutubePost{
    var URL : String
    var postedBy : String
    var postTitle : String
    var timePosted : String
    
    init(URL: String, postedBy: String, postTitle : String, timePosted : String) {
        self.URL = URL
        
        self.postedBy = postedBy
        
        self.postTitle = postTitle
        
        self.timePosted = timePosted
    }
}
