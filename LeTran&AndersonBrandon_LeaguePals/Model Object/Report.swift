//
//  Report.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/22/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation

struct Report {
    
    // MARK: Stored properties
    var reportCounter: Int
    var availReason: [String: Int]
    var typeReason: [String]
    
    // MARK: Init
    init(_reportCounter: Int, _availReason: [String: Int], _typeReason: [String]) {
        self.reportCounter = _reportCounter
        self.availReason = _availReason
        self.typeReason = _typeReason
    }
    
    init() {
        reportCounter = 0
        availReason = ["negativeAttitude": 0, "offensiveLanguage": 0, "verbalAbuse": 0]
        typeReason = [""]
    }
}
