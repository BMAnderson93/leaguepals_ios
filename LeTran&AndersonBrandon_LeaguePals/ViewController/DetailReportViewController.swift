//
//  DetailReportViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/25/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class DetailReportViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Outlets
    @IBOutlet weak var detailedReportTableView: UITableView!
    @IBOutlet weak var banUserButton: UIButton!
    
    // MARK: Properties
    var ref = Database.database().reference()
    var reportedPersonUID: String!
    var report: [(reason: String, details: String)] = []
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        banUserButton.layer.cornerRadius = banUserButton.frame.height / 2
        if reportedPersonUID == nil {
            reportedPersonUID = ""
        }
        detailedReportTableView.rowHeight = UITableViewAutomaticDimension
        detailedReportTableView.estimatedRowHeight = 500
        retrieveReports()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func banTapped(_ sender: UIButton) {
        ref.child("users").child(reportedPersonUID).child("isBanned").setValue(true)
    }
    
    // Function to retrieve the chosen user's reports
    func retrieveReports() {
        ref.child("users").child(reportedPersonUID).child("reportHistory").observe(.childAdded, with: { (snapshot) in
            if let complain = snapshot.value as? [String: String] {
                for value in complain {
                    let appendItem: (reason: String, details: String) = (value.key, value.value)
                    self.report.append(appendItem)
                }
                
                DispatchQueue.main.async {
                    self.detailedReportTableView.reloadData()
                }
            }
        }, withCancel: nil)
    }
    
    // MARK: Conform UITableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return report.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = detailedReportTableView.dequeueReusableCell(withIdentifier: "reportDetailCell", for: indexPath) as? DetailReportCell else {
            return detailedReportTableView.dequeueReusableCell(withIdentifier: "reportDetailCell", for: indexPath)
        }
        
        let subReport = report[indexPath.row]
        cell.reasonLabel.text = subReport.reason
        cell.reasonLabel.numberOfLines = 0
        cell.detailedReasonTextView.text = subReport.details
        
        return cell
    }
    
}
