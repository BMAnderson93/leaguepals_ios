//
//  ViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/10/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import UserNotifications

class ViewController: UIViewController {
    
    var currentUser : User!
    var userProfile : SummonerProfile!
    var ref : DatabaseReference!
    
    
    //This changes daily (I plan to just pass this 1 API variable in between the screens but havent done that yet)
    let apiKey = "RGAPI-efc87498-0427-4deb-b22e-fce88a67ef89"
    

    @IBOutlet weak var userNameTxtFld: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var loginStatusLabel: UILabel!
    @IBOutlet weak var doneButton: NewButton!
    @IBOutlet weak var signUpButton: NewButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        ref = Database.database().reference()
       
        for button in [doneButton, signUpButton] {
            button?.layer.cornerRadius = (button?.frame.height)! / 2
        }
     
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // This is where function to handle when the user presses the log in button
   @IBAction func LoginPress(){
        
        // Checks to make sure both of the fields have some text in them
        if !(userNameTxtFld.text?.isEmpty)! && !(passwordTxtFld.text?.isEmpty)! {
            
            let username = userNameTxtFld.text!
            let password = passwordTxtFld.text!
         
            Auth.auth().fetchProviders(forEmail: self.userNameTxtFld.text!) { (providers, error) in
                if providers == nil {
                    self.loginStatusLabel.text = "Email doesn't exist. Please register first"
                    self.loginStatusLabel.isHidden = false
                } else {
                    Auth.auth().signIn(withEmail: username, password: password) { (authResult, error) in
                        if let u = authResult?.user {
                            self.currentUser = u
                            print("Success")
                           
                            self.CreateProfile()
                        }
                        else {
                            self.loginStatusLabel.text = "Please check your email/password again"
                            self.loginStatusLabel.isHidden = false
                            print("Failed")
                        }
                    }
                }
            }
        } else {
            loginStatusLabel.text = "Please fill in all fields"
            loginStatusLabel.isHidden = false
            
    }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let svc = segue.destination as? ProfileViewController{
            svc.activeUser = currentUser
            svc.userProfile = userProfile
            svc.apiKey = apiKey
        }
        if let svc = segue.destination as? SignUpViewController{
            svc.apiKey = apiKey
        }
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        loginStatusLabel.isHidden = true
    }
    @IBAction func signUpTapped(_ sender: NewButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
}

