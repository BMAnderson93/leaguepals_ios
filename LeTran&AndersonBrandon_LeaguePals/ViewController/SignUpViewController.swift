//
//  SignUpViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/11/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate {

    var createdUser : User!
    var createdProfile : SummonerProfile!
    var ref : DatabaseReference!
    
    //This changes daily
    var apiKey = "RGAPI-efc87498-0427-4deb-b22e-fce88a67ef89"
    
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var summonerNameTxtField: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var conPasswordTxtFld: UITextField!
    @IBOutlet weak var signUpStatusLabel: UILabel!
    @IBOutlet weak var doneButton: NewButton!
    @IBOutlet weak var logInButton: NewButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for button in [doneButton, logInButton] {
            button?.layer.cornerRadius = (button?.frame.height)! / 2
        }
        
        //Creates the reference to the database which we need when creating a new user
        ref = Database.database().reference()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SignUpPressed(){
        
        //Checking to make sure all fields have text
        if !(emailTxtFld.text?.isEmpty)! && !(summonerNameTxtField.text?.isEmpty)!  && !(passwordTxtFld.text?.isEmpty)! &&
            !(conPasswordTxtFld.text?.isEmpty)! {
            
            // Check if email already exists or not
            Auth.auth().fetchProviders(forEmail: emailTxtFld.text!) { (providers, error) in
                if let _ = providers {
                    self.signUpStatusLabel.text = "Email alreasy exists or banned. Please use other email"
                    self.signUpStatusLabel.isHidden = false
                } else {
                    if self.passwordTxtFld.text == self.conPasswordTxtFld.text{
                        self.CreateNewUser(summonerName: self.summonerNameTxtField.text!, email: self.emailTxtFld.text!, password: self.passwordTxtFld.text!)
                    } else {
                        self.signUpStatusLabel.text = "Password does not match"
                        self.signUpStatusLabel.isHidden = false
                    }
                }
            }
        } else {
            signUpStatusLabel.text = "Please fill in all fields"
            signUpStatusLabel.isHidden = false
        }
    }
    
    
    //Creates the auth information for the user (which is their sign in information)
    func CreateNewUser(summonerName: String, email: String, password: String){
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            
            if let u = authResult?.user{
                self.createdUser = u
                
                print(self.createdUser.uid)
                print("Success")
                //Runs the function for creating a profile tied to this log in
                self.CreateProfile()
            }
            else {
                print("Failed")
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let svc = segue.destination as? ProfileViewController{
            //Sends the active user and their profile to the summoner profile view
            svc.activeUser = createdUser
            svc.userProfile = createdProfile
            svc.apiKey = apiKey
            
        }
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        signUpStatusLabel.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
}
