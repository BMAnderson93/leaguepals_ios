//
//  LookForGroupViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/13/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseMessaging
import UserNotifications
import Alamofire

class LookForGroupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var btn_BackToProfile: UIButton!
    @IBOutlet weak var lfgTableView: UITableView!
    @IBOutlet weak var joinedView: UIView!
    
    
    var ref: DatabaseReference!
    var truePosts: [Post] = []
    var currentRank: String!
    var postCollection: [Post] = []
    var roleViewTag: [Int] = [0, 1, 2, 3, 4]
    var chosenTag: Int!
    var activeUser: User!
    var currentProfile: SummonerProfile!
    let allRanks = ["bronze", "silver", "gold", "platinum", "diamond", "master", "challenger"]
    let allRoles = ["top", "jungle", "mid", "adc", "support"]
    var selectedRole = 0
    var apiKey = "RGAPI-efc87498-0427-4deb-b22e-fce88a67ef89"
    
    var joinedGroupHostID = ""
    
    var thisran = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Make the icon fit in the Back button
        for button in [btn_BackToProfile] {
            button?.imageView?.contentMode = .scaleAspectFit
        }
            
        ref = Database.database().reference()
        
        ref.child("LFG_Posts").observe(.childAdded) { (snapshot) in
            let posterUID = snapshot.key
            if let value = snapshot.value as? [String: Any] {
                guard let gameMode = value["Game_Mode"] as? String, let mic = value["Mic"] as? String, let poster = value["Posted_By"] as? String, let rankRequirement = value["Rank_Requirements"] as? String, let roles = value["Roles Needed"] as? [String], let timestamp = value["Time"] as? String else {
                    print("Oh snap...")
                    return
                }
                let post = Post(_roles: roles, _poster: poster, _gameMode: gameMode, _rankRequirement: rankRequirement, _mic: mic, _posterUID: posterUID, _time: timestamp)
                self.postCollection.append(post)
                
                DispatchQueue.main.async {
                    for p in self.postCollection{
                        self.thisran += 1
                        print(self.thisran)
                        var rankNeeded = -1
                        if let hasRankRequirment = self.allRanks.index(of: p.rankRequirement.lowercased()){
                            rankNeeded = hasRankRequirment
                        }
                        
                        if rankNeeded == -1 || rankNeeded <= self.allRanks.index(of: self.currentRank)! {
                            
                            var repeated = false
                            if p.posterUID != self.activeUser.uid{
                            for tp in self.truePosts {
                                if tp.posterUID == p.posterUID{
                                    repeated = true
                                    return
                                }
                                
                                }
                            if !repeated {
                                self.truePosts.append(p)
                            }
                            
                            }
                            
                        }
                    }
                    print(self.truePosts.count)
                    self.lfgTableView.reloadData()
                }
            } else {
                print("oh god")
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function to return the number of posts which are retrieved from Firebase
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return truePosts.count
    }
    
    // Function to deserialize data from Firebase into Post data and populate the tableView cells with Post data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = lfgTableView.dequeueReusableCell(withIdentifier: "lookForGroupCell", for: indexPath) as? LookForGroupCell else {
            return lfgTableView.dequeueReusableCell(withIdentifier: "lookForGroupCell", for: indexPath)
        }
        let retrievedPost = truePosts[indexPath.row]
      
        cell.gameModeLabel.text = retrievedPost.gameMode
        cell.rankRequirementLabel.text = retrievedPost.rankRequirement
        cell.posterNameLabel.setTitle("• Posted by \(retrievedPost.poster)", for: .normal)
        cell.posterNameLabel.tag = indexPath.row
        cell.dateTimeLabel.text = retrievedPost.time
        
        for role in retrievedPost.roles {
            switch role.lowercased() {
            case "top":
                cell.TopButton.isEnabled = true
                findIndex(_tag: 0)
            case "jung":
                cell.JungButton.isEnabled = true
                findIndex(_tag: 1)
            case "mid":
                cell.MidButton.isEnabled = true
                findIndex(_tag: 2)
            case "adc":
                cell.ADCButton.isEnabled = true
                findIndex(_tag: 3)
            case "sup":
                cell.SupButton.isEnabled = true
                findIndex(_tag: 4)
            default:
                print("Oh my my")
            }
        }
        
        for tag in roleViewTag {
            for view in cell.roleViewCollection {
                if view.tag == tag {
                    view.backgroundColor = UIColor.lightGray
                }
            }
        }
        
        roleViewTag = [0, 1, 2, 3, 4]
        
        switch retrievedPost.mic.lowercased() {
        case "no":
            cell.micImageView.image = #imageLiteral(resourceName: "GreyMicIcon")
        case "yes":
            cell.micImageView.image = #imageLiteral(resourceName: "MicIcon")
        default:
            print("Oh no no no no")
        }
        
        return cell
        
    }

    // Function to find the index of the tag in roleViewTag array
    func findIndex(_tag: Int) {
        for (offset, tag) in roleViewTag.enumerated() {
            if tag == _tag {
                roleViewTag.remove(at: offset)
            }
        }
    }
    
   
    
    // Function to assign the UID of the chosen player to the chosenTag
    @IBAction func toOthersProfile(_ sender: UIButton) {
        chosenTag = sender.tag
        performSegue(withIdentifier: "toOtherPlayer", sender: sender)
    }
    
    // Function to send the chosen player UID to Other Player Profile to pull their profile from Firebase
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? OtherPlayerProfileViewController {
            destination.otherPlayerUID = truePosts[chosenTag].posterUID
            print(postCollection[chosenTag].posterUID)
            
            destination.currentUser = activeUser
            destination.currentProfile = currentProfile
            destination.apiKey = apiKey
        }
    }
    
    // Function to go back to ProfileViewController
    @IBAction func backToProfile(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ChangeRole(_ sender: UIButton) {
        guard let cell = sender.superview?.superview?.superview as? LookForGroupCell else {
            return // or fatalError() or whatever
        }
        
        let indexPath = lfgTableView.indexPath(for: cell)
        
        selectedRole = sender.tag
        let sendMe = [currentRank, currentProfile.summonerName, allRoles[selectedRole]]
        ref.child("LFG_Posts").child(truePosts[(indexPath?.row)!].posterUID).child("Players").child(activeUser.uid).setValue(sendMe)
            ref.child("users").child(currentProfile.appID).child("hasGroup").setValue(true)
            currentProfile.hasJoinedGroup = true
        
        
        print("The user is currently a \(allRoles[selectedRole])")
        
        ref.child("users").child(currentProfile.appID).child("groupJoined").updateChildValues([truePosts[(indexPath?.row)!].posterUID: 1])
        //print(postCollection[(indexPath?.row)!])
        
        
        
       
        self.dismiss(animated: true, completion: nil)
        
       
    }
}
