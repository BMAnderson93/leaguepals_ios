//
//  ManageReportViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/24/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import  FirebaseAuth

class ManageReportViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var manageReportTableView: UITableView!
    
    // MARK: Properties
    var reportedUser: [SummonerProfile] = []
    var ref : DatabaseReference!
    var chosenUser: SummonerProfile!
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        backButton.imageView?.contentMode = .scaleAspectFit
    }

    override func viewWillAppear(_ animated: Bool) {
        reportedUser.removeAll()
        getReportedUsers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backToProfile(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Conform UITableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportedUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = manageReportTableView.dequeueReusableCell(withIdentifier: "reportCell", for: indexPath) as? ReportCell else {
            return manageReportTableView.dequeueReusableCell(withIdentifier: "reportCell", for: indexPath)
        }
        
        let reportedPerson = reportedUser[indexPath.row]
        cell.reportedPersonLabel.text = "• \(reportedPerson.summonerName)"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chosenUser = reportedUser[indexPath.row]
        performSegue(withIdentifier: "toDetailedReport", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailReportViewController {
            destination.reportedPersonUID = chosenUser.appID
        }
    }
}
