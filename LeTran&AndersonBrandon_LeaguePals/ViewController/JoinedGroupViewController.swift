//
//  JoinedGroupViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/24/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class JoinedGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var joinedPostTableView: UITableView!
    @IBOutlet weak var leaveGroupButton: UIButton!
    
    // MARK: Properties
    var joinedPosts: [Post] = []
    var removeGroup: [Int] = []
    var isInEdit: Bool = false
    var ref = Database.database().reference()
    var currentUser : User!
    var currentProfile : SummonerProfile!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backButton.imageView?.contentMode = .scaleAspectFit
        leaveGroupButton.imageView?.contentMode = .scaleAspectFit
        leaveGroupButton.layer.cornerRadius = leaveGroupButton.frame.height / 2
        retrieveJoinedPost()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToLookForGroup(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Function to hide back button and edit button

    
    // Function to remove the selected posts from Firebase and stored array
  
    @IBAction func leaveGroup(_ sender : UIButton){
        ref.child("LFG_Posts").child(joinedPosts[0].posterUID).child("Players").child(currentUser.uid).removeValue()
        print(joinedPosts[0].posterUID)
        ref.child("users").child(currentUser.uid).child("hasGroup").setValue(false)
        ref.child("users").child(currentUser.uid).child("groupJoined").child(joinedPosts[0].posterUID).removeValue()
        currentProfile.hasJoinedGroup = false
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // Function to hide Leave and Done buttons if user taps on Done

    
    // MARK: Conform UITableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return joinedPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = joinedPostTableView.dequeueReusableCell(withIdentifier: "joinedGroupCell", for: indexPath) as? LookForGroupCell else {
            return joinedPostTableView.dequeueReusableCell(withIdentifier: "joinedGroupCell", for: indexPath)
        }
        
        let joined = joinedPosts[indexPath.row]
        for button in [cell.MidButton, cell.JungButton, cell.SupButton, cell.TopButton, cell.ADCButton] {
            button?.isUserInteractionEnabled = false
        }
        
        for role in joined.roles {
            switch role {
                case "TOP":
                cell.TopButton.backgroundColor = UIColor.darkGray
                case "JNG":
                cell.JungButton.backgroundColor = UIColor.darkGray
                case "MID":
                cell.MidButton.backgroundColor = UIColor.darkGray
                case "ADC":
                cell.ADCButton.backgroundColor = UIColor.darkGray
                case "SUPP":
                cell.SupButton.backgroundColor = UIColor.darkGray
                default:
                print("Jeezzzzz cheez it!")
            }
        }
        
        cell.gameModeLabel.text = joined.gameMode
        cell.posterNameLabel.setTitle("\(joined.poster)", for: .normal)
        if joined.mic.lowercased() == "yes" {
            cell.micImageView.image = #imageLiteral(resourceName: "MicIcon")
        } else {
            cell.micImageView.image = #imageLiteral(resourceName: "GreyMicIcon")
        }
        
        return cell
    }
    
    // Function to retrieve the post that current user has joined
    func retrieveJoinedPost() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        ref.child("users").child(uid).child("groupJoined").observeSingleEvent(of: .value, with: {(snapshot) in
            
            print(snapshot.key)
            
        })
        ref.child("users").child(uid).child("groupJoined").observeSingleEvent(of: .value, with: {(snapshot) in
            let groupJoined = snapshot.value as? [String : Any]
            var key = ""
            if groupJoined != nil {
                for group in groupJoined! {
                    key = group.key
                }
            }
            
            if key != "" {
                self.ref.child("LFG_Posts").child(key).observeSingleEvent(of: .value, with: { (_snapshot) in
                    if let value = _snapshot.value as? [String: Any] {
                        guard let gameMode = value["Game_Mode"] as? String, let mic = value["Mic"] as? String, let poster = value["Posted_By"] as? String, let rankRequirement = value["Rank_Requirements"] as? String, let roles = value["Roles Needed"] as? [String], let timestamp = value["Time"] as? String else {
                            print("Oh snap...")
                            return
                        }
                        let post = Post(_roles: roles, _poster: poster, _gameMode: gameMode, _rankRequirement: rankRequirement, _mic: mic, _posterUID: key, _time: timestamp)
                        self.joinedPosts.append(post)
                        
                        DispatchQueue.main.async {
                            self.joinedPostTableView.reloadData()
                        }
                    }
                }, withCancel: nil)
            } else {
                self.leaveGroupButton.isHidden = true
            }
        })
        
    }
    
}
