//
//  ChatLogViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/16/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import  FirebaseAuth

class ChatLogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    // MARK: Outlets
    @IBOutlet weak var recipientNameLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var textEnterTextView: UITextView!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: Properties
    let ref = Database.database().reference()
    var recipientUID: String!
    var recipientName: String!
    var currentUser: User!
    var currentProfile: SummonerProfile!
    var messageCollection: [Message] = []
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if recipientName != nil {
            recipientNameLabel.text = recipientName
        }
        backButton.imageView?.contentMode = .scaleAspectFit
        messageTableView.rowHeight = UITableViewAutomaticDimension
        messageTableView.estimatedRowHeight = 500
        
        retrieveMessage()
        
        // Register notification to the system
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(sender:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: .UIKeyboardWillHide, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = true
        self.messageTableView.addGestureRecognizer(tapGesture)
        textEnterTextView.textColor = UIColor.darkGray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Remove notification when view disappears
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    // Function to dismiss this view controller when user taps on back button
    @IBAction func goBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Conform tableView delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageCollection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = messageTableView.dequeueReusableCell(withIdentifier: "chatLogCell", for: indexPath) as? ChatLogCell else {
            return messageTableView.dequeueReusableCell(withIdentifier: "chatLogCell", for: indexPath)
        }
        
        let message = messageCollection[indexPath.row]
        if message.senderUID == Auth.auth().currentUser?.uid {
            cell.messageLabel.textColor = UIColor.white
        } else {
            cell.messageLabel.textColor = UIColor(hexCode: "#E0872B")
        }
        cell.messageLabel.text = message.message
        cell.messageLabel.numberOfLines = 0
        
        let timestampDate = Date(timeIntervalSince1970: Double(message.timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        cell.dateTimeLabel.text = dateFormatter.string(from: timestampDate)
        
        return cell
    }
    
    // Code to push the messages to Firebase
    @IBAction func sendTapped(_ sender: UIButton) {
        if !textEnterTextView.text.isEmpty {
            let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
            let childRef = ref.child("messages").childByAutoId()
            if let recipientName = recipientName, let text = textEnterTextView.text, let senderUID = Auth.auth().currentUser?.uid {
                let values = ["from": senderUID, "text": text, "timestamp": timestamp, "to": recipientUID!, "receiverName": recipientName, "senderName": currentProfile.summonerName] as [String : Any]
                childRef.updateChildValues(values) { (error, ref_2) in
                    if error != nil {
                        print(error.debugDescription)
                        return
                    }
                    
                    let userMessagesRef = Database.database().reference().child("user-messages").child(senderUID).child(self.recipientUID)
                    let messageId = childRef.key
                    userMessagesRef.updateChildValues([messageId: 1])
                    
                    let recipientRef = self.ref.child("user-messages").child(self.recipientUID).child(senderUID)
                    recipientRef.updateChildValues([messageId: 0])
                }
            }
            
            readAllMessages()
        }
        
        textEnterTextView.text = ""
    }
    
    // Function to retrieve all the current user's messages
    func retrieveMessage() {
        if let uid = Auth.auth().currentUser?.uid {
            messageCollection.removeAll()
            ref.child("user-messages").child(uid).child(recipientUID).observe(.childAdded, with: { (snapshot) in
                self.ref.child("messages").child(snapshot.key).observeSingleEvent(of: .value, with: { (_snapshot) in
                    if let _value = _snapshot.value as? [String: Any] {
                        guard let senderUID = _value["from"] as? String, let text = _value["text"] as? String, let timestamp = _value["timestamp"] as? Int, let recipientUID = _value["to"] as? String, let recipientName = _value["receiverName"] as? String, let senderName = _value["senderName"] as? String else {
                            print("Jesus...")
                            return
                        }
                        
                        let message = Message(_message: text, _timestamp: timestamp, _recipientUID: recipientUID, _recipientName: recipientName, _senderUID: senderUID, _senderName: senderName)
                        if senderUID == uid || recipientUID == uid {
                            self.messageCollection.append(message)
                            
                            DispatchQueue.main.async {
                                self.messageTableView.reloadData()
                                self.view.layoutIfNeeded()
                            }
                        }
                    }
                }, withCancel: nil)
            }, withCancel: nil)
            
            readAllMessages()
        }
    }
    
    func readAllMessages() {
        if let uid = Auth.auth().currentUser?.uid {
            ref.child("user-messages").child(uid).child(recipientUID).observe(.value, with: { (snapshot) in
                if let value = snapshot.value as? [String: Int] {
                    let keys = Array(value.keys)
                    for subKey in keys {
                        self.ref.child("user-messages").child(uid).child(self.recipientUID).updateChildValues([subKey: 1], withCompletionBlock: { (error, ref) in
                            if error != nil {
                                print("Oh god I wanna die")
                                return
                            }
                        })
                    }
                }
            }, withCancel: nil)
        }
    }
    
    // Function to update bottom constraint of the bottom view so the user can see the textView when the keyboard shows up. Dismiss the keyboard the return the constraint back to normal
    @objc func keyBoardWillShow(sender: Notification) {
        if let userInfo = sender.userInfo as NSDictionary? {
            if let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as? NSValue {
                self.bottomViewBottomConstraint.constant -= keyboardFrame.cgRectValue.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(sender: Notification) {
        bottomViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func dismissKeyboard() {
        self.textEnterTextView.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    // MARK: Implement textView delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.textEnterTextView.becomeFirstResponder()
        if textEnterTextView.textColor == UIColor.darkGray {
            textEnterTextView.text = ""
            textEnterTextView.textColor = UIColor(hexCode: "#2B2843")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.textEnterTextView.resignFirstResponder()
        bottomViewHeightConstraint.constant = 0
        self.view.endEditing(true)
        
        if textEnterTextView.text == "" {
            textEnterTextView.text = "Say something..."
            textEnterTextView.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let lineHeight = textEnterTextView.font?.lineHeight {
            let numOfLines = Int(textEnterTextView.contentSize.height / lineHeight)
            if numOfLines < 6, numOfLines > 1 {
                bottomViewHeightConstraint.constant = lineHeight
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
        if textView.text == "" {
            bottomViewHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
}
