//
//  PreviewViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/26/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Outlets
    @IBOutlet var viewCollection: [UIView]!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var overallView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var captainTeemoImageView: UIImageView!
    @IBOutlet weak var postTableView: UITableView!
    @IBOutlet weak var feedTableView: UITableView!
    
    // MARK: Properties
    var counter: Int = 0
    let posts: [(name: String, date: String, roles: Int, rank: String, gameMode: String, mic: UIImage)] = [("Theo", "07-20-2018 8:09 PM", 1, "Bronze", "Ranked", #imageLiteral(resourceName: "MicIcon")), ("MyNameIs", "07-22-2018 10:30 PM", 4, "Diamond", "Ranked", #imageLiteral(resourceName: "MicIcon")), ("Milage", "07-27-2018 9:40 PM", 2, "None", "Unranked",#imageLiteral(resourceName: "GreyMicIcon"))]
    let feeds: [(title: String, date: String, poster: String, image: UIImage)] = [("Hey, check this out", "07-25-2018 9:47 AM", "William", #imageLiteral(resourceName: "YoutubeScreenshot")), ("Hey, check this out", "07-25-2018 9:47 AM", "William", #imageLiteral(resourceName: "YoutubeScreenshot"))]
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

       
        // Do any additional setup after loading the view.
        getStartedButton.layer.cornerRadius = getStartedButton.frame.height / 2
        captainTeemoImageView.layer.cornerRadius = captainTeemoImageView.frame.height / 2 + 1
        
        let leftSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipe(sender:)))
        leftSwipeGesture.direction = UISwipeGestureRecognizerDirection.left
        let rightSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipe(sender:)))
        rightSwipeGesture.direction = UISwipeGestureRecognizerDirection.right
        self.overallView.addGestureRecognizer(leftSwipeGesture)
        self.overallView.addGestureRecognizer(rightSwipeGesture)
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            print("Not first launch.")
            performSegue(withIdentifier: "ToLogin", sender: nil)
        } else {
            print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
    }
    
    @objc func swipe(sender: UISwipeGestureRecognizer) {
        
            if sender.direction == .right {
                if counter > 0 {
                    counter -= 1
                }
            } else if sender.direction == .left {
                if counter < 3 {
                    counter += 1
                }
            }

            print(counter)

            switch counter {
            case 0:
                hideOrUnhide(title: "PROFILE", desc: "You can see your important stats in your profile right when you log in or sign up", isLabelHidden: false, isBottomViewHidden: false, isButtonHidden: true)
            case 1:
                hideOrUnhide(title: "LOOK FOR GROUP", desc: "You are able to look for a group based on your criteria through matchmaking system", isLabelHidden: false, isBottomViewHidden: false, isButtonHidden: true)
                self.postTableView.reloadData()
            case 2:
                hideOrUnhide(title: "NEWS FEED", desc: "You can also follow your friends and watch the videos they post on the timeline", isLabelHidden: false, isBottomViewHidden: false, isButtonHidden: true)
                self.feedTableView.reloadData()
            case 3:
                hideOrUnhide(title: "", desc: "", isLabelHidden: true, isBottomViewHidden: true, isButtonHidden: false)
            default:
                print("CRAP")
            }
        
        
            UIView.animate(withDuration: 0.3) {
                self.pageControl.currentPage = self.counter
            }

            for (offset, view) in viewCollection.enumerated() {
                if offset == counter {
                    UIView.animate(withDuration: 0.3) {
                        self.pageControl.currentPage = self.counter
                        view.alpha = 1
                    }
                } else {
                    view.alpha = 0
                }
            }
    }
    
    func hideOrUnhide(title: String, desc: String, isLabelHidden: Bool, isBottomViewHidden: Bool, isButtonHidden: Bool) {
        viewTitleLabel.attributedText = NSAttributedString(string: title, attributes: [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        descriptionTextView.text = desc
        viewTitleLabel.isHidden = isLabelHidden
        bottomView.isHidden = isBottomViewHidden
        getStartedButton.isHidden = isButtonHidden
    }
    
    // MARK: Conform UITableView methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnNum: Int = 0
        if tableView == postTableView {
            returnNum = posts.count
        } else {
            returnNum = feeds.count
        }
        
        return returnNum
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if tableView == postTableView {
            guard let _cell = postTableView.dequeueReusableCell(withIdentifier: "lookForGroupCell", for: indexPath) as? LookForGroupCell else {
                return postTableView.dequeueReusableCell(withIdentifier: "lookForGroupCell", for: indexPath)
            }
            
            let object = posts[indexPath.row]
            _cell.posterNameLabel.setTitle("• \(object.name)", for: .normal)
            _cell.dateTimeLabel.text = object.date
            _cell.gameModeLabel.text = object.gameMode
            _cell.rankRequirementLabel.text = object.rank
            _cell.micImageView.image = object.mic
            
            switch object.roles {
            case 1:
                _cell.TopButton.backgroundColor = UIColor.lightGray
            case 2:
                _cell.JungButton.backgroundColor = UIColor.lightGray
            case 3:
                _cell.MidButton.backgroundColor = UIColor.lightGray
            case 4:
                _cell.ADCButton.backgroundColor = UIColor.lightGray
            case 5:
                _cell.SupButton.backgroundColor = UIColor.lightGray
            default:
                print("Oh yeah so done")
            }
            
            cell = _cell
        } else {
            guard let _cell = feedTableView.dequeueReusableCell(withIdentifier: "thirdCustomCell", for: indexPath) as? OtherNewsFeedCell else {
                return feedTableView.dequeueReusableCell(withIdentifier: "thirdCustomCell", for: indexPath)
            }
            
            let object = feeds[indexPath.row]
            _cell.dateTimeLabel.text = object.date
            _cell.postTitleLabel.text = "• \(object.title)"
            _cell.postAuthorLabel.text = "Posted by \(object.poster)"
            _cell.youtubeImageView.image = object.image
            
            cell = _cell
        }
        
        return cell
    }
}
