//
//  ManageGroupViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/19/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ManageGroupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: Outlets
    @IBOutlet weak var JungPlayerButton: UIButton!
    @IBOutlet weak var MidPlayerButton: UIButton!
    @IBOutlet weak var ADCPlayerButton: UIButton!
    @IBOutlet weak var SupPlayerButton: UIButton!
    @IBOutlet weak var TopPlayerButton: UIButton!
    @IBOutlet weak var PlayersTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var TopUncheckButton: UIButton!
    @IBOutlet weak var JNGUncheckButton: UIButton!
    @IBOutlet weak var MidUncheckButton: UIButton!
    @IBOutlet weak var ADCUncheckButton: UIButton!
    @IBOutlet weak var SupUncheckButton: UIButton!
    @IBOutlet weak var deleteGroupButton: UIButton!
    
    var ref : DatabaseReference!
    var apiKey = ""
    var currentUser : User!
    var currentProfile : SummonerProfile!
    var playersUID : [String] = []
    var playerRole : [String] = []
    var playerName : [String] = []
    var playerRank : [String] = []
    var rolesNeeded : [String] = []
    var approvedPlayers : [String : Any] = [:]
    var uidToSend = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        backButton.imageView?.contentMode = .scaleAspectFit
        deleteGroupButton.layer.cornerRadius = deleteGroupButton.frame.height / 2
        
        // Do any additional setup after loading the view.
        ref.child("LFG_Posts").observe(.childAdded) { (snapshot) in
            
            let posterUID = snapshot.key
            if posterUID == self.currentUser.uid{
                if let value = snapshot.value as? [String: Any] {
                    
                    if let roles = value["Roles Needed"] as? [String]{
                        for r in roles{
                            self.rolesNeeded.append(r)
                        }
                    }
                    if let players = value["Players"] as? [String : Any]{
                        for p in players{
                            if p.key != "Approved Players"{
                            self.playersUID.append(p.key)
                            if let playerInfo = p.value as? [String] {
                               self.playerRank.append(playerInfo[0])
                               self.playerName.append(playerInfo[1])
                               self.playerRole.append(playerInfo[2])
                            }
                        }
                          
                            
                            
                        }
                    }
                    else if let players = value["Approved Players"] as? [String : Any]{
                        for p in players{
                            print(p.key)
                            print(p.value)
                            self.approvedPlayers.updateValue(p.value, forKey: p.key)
                       
                        self.UpdateRoster()
                        }
                    }
                    
                }
                self.PlayersTableView.reloadData()
                
                for r in self.rolesNeeded{
                    switch r.lowercased() {
                    case "top":
                        
                        self.TopPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "jung":
                       
                        self.JungPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "mid":
                       
                        self.MidPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "adc":
                       
                        self.ADCPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "sup":
                        
                        self.SupPlayerButton.setTitle("Waiting for player", for: .normal)
                        
                    default :
                        print("oooooo")
                    }
                }
               
                
                
            }
        }
        ref.child("LFG_Posts").child(currentUser.uid).child("Players").observe(.childChanged)  { (snapshot) in
            
            let posterUID = snapshot.key
            if posterUID == self.currentUser.uid{
                if let value = snapshot.value as? [String: Any] {
                    
                    if let roles = value["Roles Needed"] as? [String]{
                        for r in roles{
                            self.rolesNeeded.append(r)
                        }
                    }
                    if let players = value["Players"] as? [String : Any]{
                        for p in players{
                            if p.key != "Approved Players"{
                                self.playersUID.append(p.key)
                                if let playerInfo = p.value as? [String] {
                                    self.playerRank.append(playerInfo[0])
                                    self.playerName.append(playerInfo[1])
                                    self.playerRole.append(playerInfo[2])
                                }
                            }
                            
                            
                            
                        }
                    }
                    else if let players = value["Approved Players"] as? [String : Any]{
                        for p in players{
                            print(p.key)
                            print(p.value)
                            self.approvedPlayers.updateValue(p.value, forKey: p.key)
                            
                            self.UpdateRoster()
                        }
                    }
                    
                }
                self.PlayersTableView.reloadData()
                
                for r in self.rolesNeeded{
                    switch r.lowercased() {
                    case "top":
                        
                        self.TopPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "jung":
                       
                        self.JungPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "mid":
                        
                        self.MidPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "adc":
                      
                        self.ADCPlayerButton.setTitle("Waiting for player", for: .normal)
                    case "sup":
                        
                        self.SupPlayerButton.setTitle("Waiting for player", for: .normal)
                        
                    default :
                        print("oooooo")
                    }
                }
                
                
                
            }
        }   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updatePlayers(){
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return playersUID.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = PlayersTableView.dequeueReusableCell(withIdentifier: "invitationCell", for: indexPath) as? ManageGroupTableViewCell else {
            return PlayersTableView.dequeueReusableCell(withIdentifier: "invitationCell", for: indexPath)
        }
        
        print(playersUID.count)
        if playersUID.isEmpty != true{
     
        cell.PlayerName.setTitle(playerName[indexPath.row ], for: .normal)
        
        var rankIcon = #imageLiteral(resourceName: "BronzeBadge")
        var roleIcon = #imageLiteral(resourceName: "MIDIcon")
        switch playerRole[indexPath.row] {
        case "top":
            roleIcon = #imageLiteral(resourceName: "TOPIcon")
        case "jungle":
            roleIcon = #imageLiteral(resourceName: "JNGIcon")
        case "mid":
            roleIcon = #imageLiteral(resourceName: "MIDIcon")
        case "adc":
            roleIcon = #imageLiteral(resourceName: "ADCIcon")
        case "support":
            roleIcon = #imageLiteral(resourceName: "SUPPIcon")
        default:
            print("Woops")
        }
            switch playerRank[indexPath.row]{
            case "bronze":
                rankIcon = #imageLiteral(resourceName: "BronzeBadge")
            case "silver":
                rankIcon = #imageLiteral(resourceName: "SilverBadge")
            case "gold":
                rankIcon = #imageLiteral(resourceName: "GoldBadge")
            case "platinum":
                rankIcon = #imageLiteral(resourceName: "PlatinumBadge")
            case "diamond":
                rankIcon = #imageLiteral(resourceName: "DiamondBadge")
            case "master":
                rankIcon = #imageLiteral(resourceName: "MasterBadge")
            case "challenger":
                rankIcon = #imageLiteral(resourceName: "ChallengerBadge")
            default:
                print("Woops again")
            }
            
        cell.RoleViewImageView.image = roleIcon
        cell.RankViewImageView.image = rankIcon
        }
        return cell
        
        
    }
    
    @IBAction func RemoveApproved(_ sender : UIButton){
        switch sender.tag {
        case 2:
            approvedPlayers.removeValue(forKey: "top")
            rolesNeeded.append("TOP")
            TopUncheckButton.isHidden = true
            TopPlayerButton.isEnabled = false
            TopPlayerButton.setTitle("Waiting for player", for: .normal)
        case 3:
            approvedPlayers.removeValue(forKey: "jungle")
            rolesNeeded.append("JUNG")
            JNGUncheckButton.isHidden = true
            JungPlayerButton.isEnabled = false
            JungPlayerButton.setTitle("Waiting for player", for: .normal)
        case 4:
            approvedPlayers.removeValue(forKey: "mid")
            rolesNeeded.append("MID")
            MidUncheckButton.isHidden = true
            MidPlayerButton.isEnabled = false
            MidPlayerButton.setTitle("Waiting for player", for: .normal)
        case 5:
            approvedPlayers.removeValue(forKey: "adc")
            rolesNeeded.append("ADC")
            ADCUncheckButton.isHidden = true
            ADCPlayerButton.isEnabled = false
            ADCPlayerButton.setTitle("Waiting for player", for: .normal)
        case 6:
            approvedPlayers.removeValue(forKey: "sup")
            rolesNeeded.append("SUP")
            SupUncheckButton.isHidden = true
            SupPlayerButton.isEnabled = false
            SupPlayerButton.setTitle("Waiting for player", for: .normal)
        default:
            print("MEGA Oof")
        
        }
        UpdateRoster()
         ref.child("LFG_Posts").child(currentUser.uid).child("Approved Players").setValue(approvedPlayers)
        ref.child("LFG_Posts").child(currentUser.uid).child("Roles Needed").setValue(rolesNeeded)
    }

    @IBAction func AddOrRejectPlayer(_ sender: UIButton) {
       
        guard let cell = sender.superview?.superview as? ManageGroupTableViewCell else {
            return // or fatalError() or whatever
        }
        
        
        let indexPath = PlayersTableView.indexPath(for: cell)
        
        switch sender.tag {
        case 0:
        ref.child("LFG_Posts").child(currentUser.uid).child("Players").child(playersUID[(indexPath?.row)!]).removeValue()
            playerName.remove(at: (indexPath?.row)!)
            playerRole.remove(at: (indexPath?.row)!)
            playerRank.remove(at: (indexPath?.row)!)
            playersUID.remove(at: (indexPath?.row)!)
            PlayersTableView.deleteRows(at: [indexPath!], with: .fade)
            
        case 1:
            let playerInfo = [playerRank[(indexPath?.row)!], playerName[(indexPath?.row)!], playersUID[(indexPath?.row)!]]
            approvedPlayers.updateValue(playerInfo, forKey: playerRole[(indexPath?.row)!])
           
            ref.child("LFG_Posts").child(currentUser.uid).child("Players").child(playersUID[(indexPath?.row)!]).removeValue()
            var p = ""
           switch  playerRole[(indexPath?.row)!].lowercased()
           {
           case "top":
            p = "TOP"
           case "jungle":
            p = "JUNG"
           case "mid":
            p = "MID"
           case "adc":
            p = "ADC"
           case "sup":
            p = "SUP"
           default :
            print("UH OH")
            }
            print(p)
            
            for r in rolesNeeded{
                print(r)
            }
            let removeAt = rolesNeeded.index(of: p)
            print(rolesNeeded[removeAt!])
            rolesNeeded.remove(at: removeAt!)
            
            
            playerName.remove(at: (indexPath?.row)!)
            playerRole.remove(at: (indexPath?.row)!)
            playerRank.remove(at: (indexPath?.row)!)
            playersUID.remove(at: (indexPath?.row)!)
            PlayersTableView.deleteRows(at: [indexPath!], with: .fade)
            
            
      
        default:
          print("MEGA Oof")
        }
        ref.child("LFG_Posts").child(currentUser.uid).child("Roles Needed").setValue(rolesNeeded)
        ref.child("LFG_Posts").child(currentUser.uid).child("Approved Players").setValue(approvedPlayers)
        UpdateRoster()
        
    }
    
    func UpdateRoster(){
        
        for p in approvedPlayers{
            let playerInfo = p.value as? [String]
            switch p.key.lowercased(){
            case "top" :
                TopUncheckButton.isHidden = false
                TopPlayerButton.isEnabled = true
                TopPlayerButton.setTitle(playerInfo?[1], for: .normal)
            case "jungle" :
                JNGUncheckButton.isHidden = false
                JungPlayerButton.isEnabled = true
                JungPlayerButton.setTitle(playerInfo?[1], for: .normal)
            case "mid" :
                MidUncheckButton.isHidden = false
                MidPlayerButton.isEnabled = true
                MidPlayerButton.setTitle(playerInfo?[1], for: .normal)
            case "adc" :
                ADCUncheckButton.isHidden = false
                ADCPlayerButton.isEnabled = true
                ADCPlayerButton.setTitle(playerInfo?[1], for: .normal)
            case "sup" :
                SupUncheckButton.isHidden = false
                SupPlayerButton.isEnabled = true
                SupPlayerButton.setTitle(playerInfo?[1], for: .normal)
            default :
                print("OOF")
                
            }
        }
    }
    
    @IBAction func backToProfile(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ViewOtherProfile(_ sender: UIButton) {
        
        for p in approvedPlayers{
            
            if let d = p.value as? [String]{
                if d[1] == sender.titleLabel?.text{
                    uidToSend = d[2]
                    performSegue(withIdentifier: "ToOtherProfile", sender: self)
                }
                
            }
            
            
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? OtherPlayerProfileViewController {
            destination.otherPlayerUID = uidToSend
            
            destination.currentUser = currentUser
            destination.currentProfile = currentProfile
            destination.apiKey = apiKey
        }
        if let destination = segue.destination as? NewsFeedViewController{
            destination.userProfile = currentProfile
            destination.activeUser = currentUser
        }
    }

    @IBAction func DeleteGroup(_ sender: UIButton) {
        ref.child("users").child(currentUser.uid).child("hasPost").setValue(false)
        currentProfile.hasPost = false
       self.dismiss(animated: true, completion: nil)
    }
}
