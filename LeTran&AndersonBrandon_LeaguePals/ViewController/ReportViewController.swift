//
//  ReportViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/22/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class ReportViewController: UIViewController, UITextViewDelegate {

    // MARK: Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var reportTextView: UITextView!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var reportedView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet var roundButtonCollection: [UIButton]!
    @IBOutlet var otherButtonCollection: [UIButton]!
    @IBOutlet var viewCollection: [UIView]!
    
    
    // MARK: Properties
    var otherPlayerProfile: SummonerProfile!
    var tappedButton: [Int] = []
    var button1Tapped = false
    var button2Tapped = false
    var button3Tapped = false
    var viewMove: CGFloat = 0
    
   
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        reportTextView.text = "Tell us what happened..."
        reportTextView.textColor = UIColor.lightGray
        
        // Change layout of UI elements
        backButton.imageEdgeInsets = UIEdgeInsetsMake(2, 2, 2, 2)
        backButton.imageView?.contentMode = .scaleAspectFit
        
        for view in viewCollection {
            view.layer.cornerRadius = view.frame.height / 2 + 1
        }
        
        for button in [roundButtonCollection, otherButtonCollection] {
            for subButton in button! {
                subButton.layer.cornerRadius = subButton.frame.height / 2
            }
        }
        
        for button in [reportButton, cancelButton] {
            button?.layer.cornerRadius = (button?.frame.height)! / 2
        }
        
        // Create gesture to dismiss the keyboard when user taps on the topView
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = true
        self.topView.addGestureRecognizer(tapGesture)
        
        // Send notification to the center to trigger the target function of the observer
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: .UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(sender: Notification) {
        if let userInfo = sender.userInfo as NSDictionary? {
            if let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as? NSValue {
                self.viewMove = keyboardFrame.cgRectValue.height - (keyboardFrame.cgRectValue.height / 3)
                self.view.frame.origin.y -= self.viewMove
                self.view.setNeedsLayout()
            }
        }
    }
    
    @objc func keyboardWillHide(sender: Notification) {
        textViewDidEndEditing(UITextView())
    }

    @IBAction func choiceTapped(_ sender: UIButton) {
        let tag = sender.tag
        switch tag {
        case 0:
            button1Tapped = radioButtonChecked(buttonTapped: button1Tapped, tag: 0)
        case 1:
            button2Tapped = radioButtonChecked(buttonTapped: button2Tapped, tag: 1)
        case 2:
            button3Tapped = radioButtonChecked(buttonTapped: button3Tapped, tag: 2)
        default:
            print("Hello there, you screwed!")
        }
    }
    
    func radioButtonChecked(buttonTapped: Bool, tag: Int) -> Bool {
        var result: Bool = false
        if buttonTapped == false {
            for button in roundButtonCollection {
                if button.tag == tag {
                    button.backgroundColor = UIColor(hexCode: "#2B2843")
                    result = true
                }
            }
        } else {
            for button in roundButtonCollection {
                if button.tag == tag {
                    button.backgroundColor = UIColor.white
                }
            }
        }
        
        return result
    }
    
    @IBAction func reportTapped(_ sender: UIButton) {
        if button1Tapped || button2Tapped || button3Tapped || reportTextView.textColor == UIColor(hexCode: "#2B2843") {
            reportedView.isHidden = false
           
            var reason = ""
            
            if button1Tapped {
               
                if reason == "" {
                    reason = "Verbal Abuse"
                }
                else {
                    reason += ", Verbal Abuse"
                }
            }
            if button2Tapped {
                
                
                if reason == "" {
                    reason = "Offensive Language"
                }
                else {
                    reason += ", Offensive Language"
                }
            }
            if button3Tapped {
               
                if reason == "" {
                    reason = "Negative Attitude"
                }
                else {
                    reason += ", Negative Attitude"
                }
            }
            
            if reportTextView.text != "Tell us what happened...", reportTextView.text != "" {
               
            }
            
            let reportRight = [reason : reportTextView.text ]
            
            let uID = Auth.auth().currentUser?.uid
            
            let sendME : [String: Any] = [uID! : reportRight]
            Database.database().reference().child("users").child(self.otherPlayerProfile.appID).child("reportHistory").updateChildValues(sendME)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Functions to dismiss keyboard
    @objc func dismissKeyboard() {
        self.reportTextView.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    // MARK: Conform UITextView delegate methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.reportTextView.becomeFirstResponder()
        if reportTextView.textColor == UIColor.lightGray {
            reportTextView.text = ""
            reportTextView.textColor = UIColor(hexCode: "#2B2843")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.reportTextView.resignFirstResponder()
        self.view.frame.origin.y = 0
        self.view.setNeedsLayout()
        
        if reportTextView.text == "" {
            reportTextView.text = "Tell us what happened..."
            reportTextView.textColor = UIColor.lightGray
        }
    }
}
