//
//  OtherPlayerProfileViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/15/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import  FirebaseAuth
class OtherPlayerProfileViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var summonerNameLabel: UILabel!
    @IBOutlet weak var summonerRankImageView: NewImageView!
    @IBOutlet weak var summonerWinLossLabel: UILabel!
    @IBOutlet weak var summonerLPLabel: UILabel!
    @IBOutlet weak var summonerProfileIconImageView: UIImageView!
    @IBOutlet weak var glowView: NewView!
    @IBOutlet weak var followButton: UIButton!
    
    //Place holder for our users profile icon id
    var profileIconID = 3507
    // All placeholder variables
    var summonerRank = "Silver"
    var summonerWins = 0
    var summonerLosses = 0
    var summonerCurrentLP = 0
    
    // MARK: Properties
    var otherPlayerUID: String!
    var apiKey = "RGAPI-efc87498-0427-4deb-b22e-fce88a67ef89"
    var ref = Database.database().reference()
    var otherPlayerIconID: Int!
    var otherPlayerID: Int!
    var otherPlayerLevel: Int!
    var otherPlayerName: String!
    var rank: String!
    var wins: Int!
    var losses: Int!
    var currentLP: Int!
    var currentUser: User!
    var currentProfile: SummonerProfile!
    var otherProfile: SummonerProfile!
    
    
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet var ratingStars: [UIImageView]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CreateProfile()
      
        // Do any additional setup after loading the view.
        for imageView in [summonerRankImageView, summonerProfileIconImageView] {
            imageView?.layer.cornerRadius = (imageView?.frame.height)! / 2 + 1
        }
        
        for button in [backButton, messageButton] {
            button?.imageView?.contentMode = .scaleAspectFit
        }
        
        glowView.layer.cornerRadius = glowView.frame.height / 2 + 1
        followButton.layer.cornerRadius = followButton.frame.height
         / 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Functions
    // Function to go back to Look For Group view when user taps on back button
    @IBAction func backToLookForGroup(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         ref.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
                if let value = snapshot.value as? NSDictionary{
                    
                    if let ratedUsers = value["ratedUsers"] as? [String : Any]{
                        for r in ratedUsers{
                            if r.key == self.otherPlayerUID{
                                DispatchQueue.main.async {
                                    self.ratingButton.isHidden = true
                                }
                            }
                        }
                    }
                    if let following = value["following"] as? [String: Any]{
                        for f in following{
                            if f.key == self.otherPlayerUID {
                                self.ref.child("users").child(self.currentUser.uid).child("following").child(self.otherPlayerUID).setValue("Following")
                                DispatchQueue.main.async {
                                    self.followButton.tag = 1
                                    self.followButton.setTitle("Unfollow", for: .normal)
                                }
                               
                            }
                        }
                    }
            }
        })
        
        if otherProfile != nil {
            GetRating()
        }
    }
    @IBAction func FolloworUnfollow(_ sender: UIButton){
        if sender.tag == 0 {
            ref.child("users").child(currentUser.uid).child("following").child(otherPlayerUID).setValue("Following")
            sender.setTitle("Unfollow", for: .normal)
            sender.tag = 1
            
        }
        else if sender.tag == 1 {
            ref.child("users").child(currentUser.uid).child("following").child(otherPlayerUID).removeValue()
            sender.setTitle("Follow", for: .normal)
            sender.tag = 0
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChatLogViewController {
            destination.recipientUID = otherPlayerUID
            destination.recipientName = otherProfile.summonerName
            destination.currentUser = currentUser
            destination.currentProfile = currentProfile
        } else if let place = segue.destination as? ReportViewController {
            place.otherPlayerProfile = otherProfile
        }
        if let svc = segue.destination as? RatingViewController{
            svc.otherPlayerUID = otherPlayerUID
            svc.otherPlayerProfile = otherProfile
        }
    }
    
    @IBAction func unwindToHere(segue: UIStoryboardSegue) {
        if let _ = segue.source as? RatingViewController {
            self.ratingButton.isHidden = true
            ref.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let value = snapshot.value as? NSDictionary{
                    
                    if let ratedUsers = value["ratedUsers"] as? [String : Any]{
                        for r in ratedUsers{
                            if r.key == self.otherPlayerUID{
                                DispatchQueue.main.async {
                                    self.ratingButton.isHidden = true
                                }
                            }
                        }
                    }
                    if let following = value["following"] as? [String: Any]{
                        for f in following{
                            if f.key == self.otherPlayerUID {
                                self.ref.child("users").child(self.currentUser.uid).child("following").child(self.otherPlayerUID).setValue("Following")
                                DispatchQueue.main.async {
                                    self.followButton.tag = 1
                                    self.followButton.setTitle("Unfollow", for: .normal)
                                }
                                
                            }
                        }
                    }
                }
            })
            
            if otherProfile != nil {
                GetRating()
            }
            
        }
    }
}
