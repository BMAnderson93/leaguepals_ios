//
//  ProfileViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/11/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth



class ProfileViewController: UIViewController {
    
    //Catches our active user from login
    var activeUser : User!
    
    //Catches our summoner profile from Signup / Login
    var userProfile : SummonerProfile!
    
    //This needs to be changed daily
    var apiKey = "RGAPI-efc87498-0427-4deb-b22e-fce88a67ef89"
    
    //Place holder for our users profile icon id
    var profileIconID = 3507
    // All placeholder variables
    var summonerRank = "Silver"
    var summonerWins = 0
    var summonerLosses = 0
    var summonerCurrentLP = 0
    var ref = Database.database().reference()
    var numOfNewMessages: [String: Int] = [:]
    
    @IBOutlet var ratingStars: [UIImageView]!
    // MARK: Outlets
    @IBOutlet weak var CreateGroupButton: NewButton!
    @IBOutlet weak var summonerRankImage: NewImageView!
    @IBOutlet weak var summonerLP: UILabel!
    @IBOutlet weak var summonerWinLoss: UILabel!
    @IBOutlet weak var summonerProfileIcon_ImageView: NewImageView!
    @IBOutlet weak var summonerName_Label: UILabel!
    @IBOutlet weak var btn_NewsFeed: UIButton!
    @IBOutlet weak var btn_Messages: UIButton!
    @IBOutlet weak var messageNotificationLabel: UILabel!
    @IBOutlet weak var manageReportButton: UIButton!
    @IBOutlet weak var LookForGroupButton: NewButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        profileIconID = userProfile.summonerIconID
        //This will build the api string we need for the profile icon of the user
        let profileIconURL = "http://ddragon.leagueoflegends.com/cdn/8.13.1/img/profileicon/\(profileIconID).png"
        GetRating()
        
        AssignProfileImage(url: profileIconURL)
        GetRankStats(summonerID: userProfile.summonerID)
        // Make the buttons' images fit and not stretched
        for button in [btn_NewsFeed, btn_Messages, manageReportButton] {
            button?.imageView?.contentMode = .scaleAspectFit
        }
        
        for button in [CreateGroupButton, LookForGroupButton] {
            button?.layer.cornerRadius = (button?.frame.height)! / 2
        }
        
        summonerProfileIcon_ImageView.layer.cornerRadius = summonerProfileIcon_ImageView.frame.height / 2 + 1
        messageNotificationLabel.layer.cornerRadius = messageNotificationLabel.frame.height / 2 + 1
        messageNotificationLabel.layer.masksToBounds = true
        
        if userProfile.isAdmin {
            manageReportButton.isHidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Sends the current user and their profile to the create a group view controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let svc = segue.destination as? CreateGroupViewController{
                svc.currentProfile = userProfile
                svc.currentUser = activeUser
            
        } else if let svc = segue.destination as? MessagesViewController {
            svc.activeUser = activeUser
            svc.currentProfile = userProfile
        } else if let svc = segue.destination as? LookForGroupViewController {
            svc.activeUser = activeUser
            svc.currentRank = summonerRank.lowercased()
            svc.currentProfile = userProfile
            svc.apiKey = apiKey
            
        }
        else if let svc = segue.destination as? JoinedGroupViewController{
            svc.currentUser = activeUser
            svc.currentProfile = userProfile
        }
        else if let svc = segue.destination as? ManageGroupViewController{
            svc.currentUser = activeUser
            svc.currentProfile = userProfile
            svc.apiKey = apiKey 
            
        }
        else if let svc = segue.destination as? NewsFeedViewController{
            svc.activeUser = activeUser
            svc.userProfile = userProfile
        }
        else if let svc = segue.destination as? FollowingViewController{
            svc.apiKey = apiKey
            svc.activeUser = activeUser
            svc.currentProfile = userProfile
        }
    }
   
    @IBAction func CreateOrManage(_ sender: UIButton) {
        if sender.tag == 1 {
        performSegue(withIdentifier: "CreateGroup", sender: self)
        }
        else{
        performSegue(withIdentifier: "ManageGroup", sender: self)
        }
    }
    
    @IBAction func LFGorJoinedGroup(_ sender: UIButton){
        if sender.tag == 1 {
            performSegue(withIdentifier: "LookForGroup", sender: self)
        }
        else {
            performSegue(withIdentifier: "JoinedGroup", sender: self)
        }
    }
    
    @IBAction func SignOut(_ sender: Any) {
        
        do{
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        }catch{
            print("Error while signing out!")
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        detectNewMessages()
        
        if userProfile.hasPost{
            CreateGroupButton.setTitle("Manage Group", for: .normal)
            CreateGroupButton.tag = 2
        }
        else {
            CreateGroupButton.setTitle("Create Group", for: .normal)
            CreateGroupButton.tag = 1
        }
        
        if userProfile.hasJoinedGroup{
            LookForGroupButton.setTitle("Joined Group", for: .normal)
            LookForGroupButton.tag = 2
        }
        else{
            LookForGroupButton.setTitle("Look For Group", for: .normal)
            LookForGroupButton.tag = 1
        }
    }
    
    // Function to check the database if there is a new message and if there is, display a badge notification on the Message button
    func detectNewMessages() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        ref.child("user-messages").child(uid).observe(.childAdded, with: { (snapshot) in
            var newMessages: [Int] = []
            var numOfMessages: Int = 0
            print(snapshot)
            if let value = snapshot.value as? [String: Int] {
                numOfMessages = value.count
            }
            self.ref.child("user-messages").child(uid).child(snapshot.key).observe(.childAdded, with: { (snapshot_1) in
                print("This is snapshot_1: \(snapshot_1)")
                
                if let value = snapshot_1.value as? Int {
                    numOfMessages -= 1
                    newMessages.append(value)
                    
                    if numOfMessages == 0 {
                        if newMessages.contains(0) {
                            self.numOfNewMessages[snapshot.key] = 0
                        } else {
                            self.numOfNewMessages[snapshot.key] = 1
                        }
                        
                        for news in self.numOfNewMessages {
                            if news.value == 1 {
                                self.numOfNewMessages.removeValue(forKey: news.key)
                            }
                        }
                    }
                    
                    
                }
                
                DispatchQueue.main.async {
                    if self.numOfNewMessages.count > 0 {
                        self.messageNotificationLabel.text = self.numOfNewMessages.count.description
                        self.messageNotificationLabel.isHidden = false
                    } else {
                        self.messageNotificationLabel.text = "0"
                        self.messageNotificationLabel.isHidden = true
                    }
                }
            }, withCancel: nil)
        }, withCancel: nil)
    }
}
