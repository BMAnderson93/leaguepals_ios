//
//  RatingViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/24/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RatingViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet var allStars: [UIButton]!
    
    // MARK: Properties
    var otherPlayerUID : String!
    var otherPlayerProfile : SummonerProfile!
    var rating = 0
    var ref : DatabaseReference!
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
         ref = Database.database().reference()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ChangeRating(_ sender : UIButton){
        rating = sender.tag
        for s in allStars{
            if s.tag <= rating{
                s.setImage(#imageLiteral(resourceName: "Star"), for: .normal)
            }
            else{
                s.setImage(#imageLiteral(resourceName: "EmptyStar"), for: .normal)
            }
        }
    }

    @IBAction func SubmitRating(_ sender: Any) {
        var allRatings : [Int] = []
       
        ref.child("users").child(otherPlayerUID).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            
             if let value = snapshot.value as? NSDictionary{
                
                allRatings = (value["ratings"] as? [Int])!
                
                DispatchQueue.main.async {
                    self.ref.child("users").child((Auth.auth().currentUser?.uid)!).child("ratedUsers").child(self.otherPlayerUID).setValue(self.rating)
                    allRatings.append(self.rating)
                    self.ref.child("users").child(self.otherPlayerUID).child("ratings").setValue(allRatings)
                    self.otherPlayerProfile.ratings = allRatings
                    
                }
            }
        })
        
        performSegue(withIdentifier: "backToOther", sender: self)
    }
}
