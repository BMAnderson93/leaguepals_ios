//
//  CreateGroupViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/13/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class CreateGroupViewController: UIViewController {

    //All settings the user can choose from when making a post
    let rankRequirements = ["None","Bronze","Silver","Gold","Platinum", "Diamond", "Master", "Challenger"]
    let gameModes = ["Unranked", "Ranked"]
    let mic = ["Yes", "No"]
    
    //Used for keeping track of all needed roles
    var neededRoles : [String] = []
    //These are used for keeping track of what settings are selected
    var rankSelection : Int!
    var gameModeSelection : Int!
    var micSelection : Int!
    //Ref to the database
    var ref : DatabaseReference!
    //Reference to the currently logged on user and their summoner profile
    var currentUser : User!
    var currentProfile : SummonerProfile!
    
    //Reference to all the labels buttons and views used for this app. The Hide views are what give the black transparent view to the roles selected so that the user knows theyve picked them
    @IBOutlet weak var MicRequirmentLabel: UILabel!
    @IBOutlet weak var GameModeLabel: UILabel!
    @IBOutlet weak var RankRequirementLabel: UILabel!
    @IBOutlet weak var HideTop: UIView!
    @IBOutlet weak var HideJung: UIView!
    @IBOutlet weak var HideMid: UIView!
    @IBOutlet weak var HideAdc: UIView!
    @IBOutlet weak var HideSup: UIView!
    @IBOutlet var backwardForwardButtons: [UIButton]!
    @IBOutlet weak var btn_BackToProfile: UIButton!
    @IBOutlet weak var postedNotificationView: UIView!
    @IBOutlet weak var warningLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         ref = Database.database().reference()
        // Make the icon fit in the buttons
        for button in backwardForwardButtons {
            button.imageView?.contentMode = .scaleAspectFit
            button.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        }
        btn_BackToProfile.imageView?.contentMode = .scaleAspectFit
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Adds roles to the roles needed array based on what button is hit
    @IBAction func RoleSelected(_ sender: UIButton) {
        
        // Hide the warning label if it is not hidden
        if !warningLabel.isHidden {
            warningLabel.isHidden = true
        }
        
        //We will either hide or show that extra view made to give the effect that a role has been selected and give feedback to the user. We will also add or remove that role from the list of needed roles based on wether the user is selecting or deselecting the role.
        switch sender.tag {
        case 0:
            if HideTop.isHidden{
                HideTop.isHidden = false
                neededRoles.append("TOP")
            }
            else{
                HideTop.isHidden = true
                neededRoles.remove(at: neededRoles.index(of: "TOP")!)
                
            }
        case 1:
            if HideJung.isHidden{
                HideJung.isHidden = false
                neededRoles.append("JUNG")
                
            }
            else{
                HideJung.isHidden = true
                neededRoles.remove(at: neededRoles.index(of: "JUNG")!)
            }
        case 2:
            if HideMid.isHidden{
                HideMid.isHidden = false
                neededRoles.append("MID")
            }
            else{
                HideMid.isHidden = true
                neededRoles.remove(at: neededRoles.index(of: "MID")!)
            }
        case 3:
            if HideAdc.isHidden{
                HideAdc.isHidden = false
                neededRoles.append("ADC")
            }
            else{
                HideAdc.isHidden = true
                neededRoles.remove(at: neededRoles.index(of: "ADC")!)
            }
        case 4:
            if HideSup.isHidden{
                HideSup.isHidden = false
                neededRoles.append("SUP")
            }
            else{
                HideSup.isHidden = true
                neededRoles.remove(at: neededRoles.index(of: "SUP")!)
            }
        default:
            print("Get Rekt")
        }
    }
    //This is just for setting all the requriments of the post to work with the arrows, the logic for each setting works the same so ill comment it once
    @IBAction func Settings(_ sender: UIButton){
        // Hide the warning label if it is not hidden
        if !warningLabel.isHidden {
            warningLabel.isHidden = true
        }
        
        switch sender.tag {
        case 0:
            //Based on the senders tag we know wether to show the next option in the array of options or the previous one
            if rankSelection == nil{
                rankSelection = 0
                RankRequirementLabel.text = rankRequirements[rankSelection]
            }
            //If were on the last option or the first we will go through to the end or begining based on which it is
            //Also if the selection is nil we know its the first time the user has pressed the arrow key for that setting
            else {
                rankSelection! += 1
                if rankSelection == rankRequirements.count{
                    rankSelection = 0
                }
                RankRequirementLabel.text = rankRequirements[rankSelection]
            }
            
        case 1:
            if rankSelection == nil{
                rankSelection = 0
                RankRequirementLabel.text = rankRequirements[rankSelection]
            }
            else {
                rankSelection! -= 1
                if rankSelection < 0{
                    rankSelection = rankRequirements.count - 1
                }
                RankRequirementLabel.text = rankRequirements[rankSelection]
            }
        case 2:
            if gameModeSelection == nil{
                gameModeSelection = 0
                GameModeLabel.text = gameModes[gameModeSelection]
            }
            else {
                gameModeSelection! += 1
                if gameModeSelection == gameModes.count{
                    gameModeSelection = 0
                }
                GameModeLabel.text = gameModes[gameModeSelection]
            }
            
        case 3:
            if gameModeSelection == nil{
                gameModeSelection = 0
                GameModeLabel.text = gameModes[gameModeSelection]
            }
            else {
                 gameModeSelection! -= 1
                if gameModeSelection < 0{
                    gameModeSelection = gameModes.count - 1
                }
                GameModeLabel.text = gameModes[gameModeSelection]
            }
        case 4:
            if micSelection == nil{
                micSelection = 0
                MicRequirmentLabel.text = mic[micSelection]
            }
            else {
                micSelection! += 1
                if micSelection == mic.count{
                    micSelection = 0
                }
                MicRequirmentLabel.text = mic[micSelection]
            }
            
        case 5:
            if micSelection == nil{
                micSelection = 0
                MicRequirmentLabel.text = mic[micSelection]
            }
            else {
                micSelection! -= 1
                if micSelection < 0{
                    micSelection = mic.count - 1
                }
                MicRequirmentLabel.text = mic[micSelection]
            }
        default:
            print("Gotem")
        }
    }
    
    //Used for testing sending the post information to the server (All fields must be entered or it may crash)
    @IBAction func TestPrint(){
        
        // Validation for requirements fields
        var atLeastOneRole: Bool = true
        if HideAdc.isHidden, HideMid.isHidden, HideSup.isHidden, HideTop.isHidden, HideJung.isHidden {
            atLeastOneRole = false
        }
        if MicRequirmentLabel.text?.lowercased() == "mic requirement" || GameModeLabel.text?.lowercased() == "game mode" || RankRequirementLabel.text?.lowercased() == "rank requirement" || atLeastOneRole == false {
            warningLabel.isHidden = false
        } else {
            print("We need a")
            for r in neededRoles{
                print(r)
            }
            print("Rank - \(rankRequirements[rankSelection]) GameMode - \(gameModes[gameModeSelection]) Mic - \(mic[micSelection])")
            
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
            let time = dateFormatter.string(from: date)
            
            //Creates all the information required for the post to be sent to the server
            let postInfo = ["Time": time, "Rank_Requirements" : rankRequirements[rankSelection], "Game_Mode" : gameModes[gameModeSelection], "Mic" : mic[micSelection], "Posted_By" : currentProfile.summonerName, "Roles Needed" : neededRoles] as [String : Any]
            
            
            
            //Creates and saves the post in the correct location on Firebase
            ref.child("LFG_Posts").child(currentUser.uid).setValue(postInfo)
            ref.child("users").child(currentUser.uid).updateChildValues(["hasPost" : true])
            currentProfile.hasPost = true
            
            postedNotificationView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // Function to go back to Profile screen when user taps on back button
    @IBAction func backToProfile(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
   
}

