//
//  MessagesViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/11/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class MessagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Outlets
    @IBOutlet weak var btn_BackToProfile: UIButton!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var noMessageView: UIView!
    
    // MARK: Properties
    var ref: DatabaseReference!
    var activeUser: User!
    var currentProfile: SummonerProfile!
    var messageCollection: [Message] = []
    var messageDict = [String: Message]()
    var recipientName: String!
    var recipientUID: String!
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Make the button's image fit
        btn_BackToProfile.imageView?.contentMode = .scaleAspectFit
        
        // Create reference
        ref = Database.database().reference()
    }

    override func viewWillAppear(_ animated: Bool) {
        messageDict.removeAll()
        messageCollection.removeAll()
        observeUserMessage()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageCollection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = messageTableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as? MessageCell else {
            return messageTableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath)
        }
        
        if messageCollection.count > 0 {
            let message = messageCollection[indexPath.row]
            cell.messageLabel.text = message.message
            if message.recipientUID == currentProfile.appID {
                cell.senderNameLabel.text = "• \(message.senderName)"
            } else {
                cell.senderNameLabel.text = "• \(message.recipientName)"
            }
            let timestampDate = Date(timeIntervalSince1970: Double(message.timestamp))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            cell.dateTimeLabel.text = dateFormatter.string(from: timestampDate)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chosenMessage = messageCollection[indexPath.row]
        if chosenMessage.recipientUID == Auth.auth().currentUser?.uid {
            recipientUID = chosenMessage.senderUID
            recipientName = chosenMessage.senderName
        } else {
            recipientUID = chosenMessage.recipientUID
            recipientName = chosenMessage.recipientName
        }
        performSegue(withIdentifier: "toChatLog", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChatLogViewController {
            destination.currentUser = activeUser
            destination.recipientName = recipientName
            destination.recipientUID = recipientUID
            destination.currentProfile = currentProfile
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        if let chatPartnerUID = messageCollection[indexPath.row].chatPartnerUID() {
            ref.child("user-messages").child(uid).child(chatPartnerUID).removeValue { (error, ref) in
                if error != nil {
                    print("Uh oh")
                    return
                }
                
                self.messageDict.removeValue(forKey: chatPartnerUID)
                self.reloadTable()
            }
        }
    }
    @IBAction func backToProfile(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func observeUserMessage() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        ref.child("user-messages").child(uid).observe(.childAdded, with: { (snapshot) in
            self.ref.child("user-messages").child(uid).child(snapshot.key).observe(.childAdded, with: { (snapshot) in
                self.ref.child("messages").child(snapshot.key).observeSingleEvent(of: .value, with: { (_snapshot) in
                    if let value = _snapshot.value as? [String: Any] {
                        guard let senderUID = value["from"] as? String, let text = value["text"] as? String, let timestamp = value["timestamp"] as? Int, let recipientUID = value["to"] as? String, let recipientName = value["receiverName"] as? String, let senderName = value["senderName"] as? String else {
                            print("Jesus...")
                            return
                        }
                        print("gotcha")
                        
                        let message = Message(_message: text, _timestamp: timestamp, _recipientUID: recipientUID, _recipientName: recipientName, _senderUID: senderUID, _senderName: senderName)
                        
                        if let chatPartnerUID = message.chatPartnerUID() {
                            self.messageDict[chatPartnerUID] = message
                        }
                        
                        self.reloadTable()
                    }
                }, withCancel: nil)
            }, withCancel: nil)
        }, withCancel: nil)
        
        ref.child("user-messages").child(uid).observe(.childRemoved, with: { (snapshot) in
            self.messageDict.removeValue(forKey: snapshot.key)
            self.reloadTable()
        }, withCancel: nil)
    }
    
    // Function to reload the tableView if the data changes
    func reloadTable() {
        self.messageCollection = Array(self.messageDict.values)
        self.messageCollection = self.messageCollection.sorted(by: { (m1, m2) -> Bool in
            return m1.timestamp > m2.timestamp
        })
        
        DispatchQueue.main.async {
            self.messageTableView.reloadData()
            
            if self.messageCollection.count > 0 {
                self.noMessageView.isHidden = true
            } else {
                self.noMessageView.isHidden = false
            }
        }
    }
}
