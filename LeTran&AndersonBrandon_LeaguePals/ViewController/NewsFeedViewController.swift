//
//  NewsFeedViewController.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/11/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import Firebase
import  FirebaseAuth

class NewsFeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var btn_BackToProfile: UIButton!
    @IBOutlet weak var newsFeedTableView: UITableView!
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var postViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var dismissPostViewButton: UIButton!
    @IBOutlet weak var postedView: UIView!
    @IBOutlet weak var uploadButton: UIButton!
    
    var activeUser: User!
    var userProfile: SummonerProfile!
    var allPosts: [YoutubePost] = []
    var ref : DatabaseReference!
    var allFollowed : [String] = []
    var keyboardHeight: CGFloat = 0
    var keyboardDuration: TimeInterval = 0
    
    @IBOutlet weak var URLLabel: UITextField!
    @IBOutlet weak var VideoTitleLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference()
        // Do any additional setup after loading the view.
        
        getPosts()
        
        // Make button's image fit
        btn_BackToProfile.imageView?.contentMode = .scaleAspectFit
        uploadButton.layer.cornerRadius = uploadButton.frame.height / 2
        
        // Register notification to the system
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(sender:)), name: .UIKeyboardWillShow, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Conform UITableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = newsFeedTableView.dequeueReusableCell(withIdentifier: "YouTubePostCell", for: indexPath) as? NewsFeedTableViewCell  else {
            return newsFeedTableView.dequeueReusableCell(withIdentifier: "YouTubePostCell", for: indexPath)
        }
        
        let subPost = allPosts[indexPath.row]
        cell.AuthorLabel.text = "Posted by \(subPost.postedBy)"
        cell.DatePostedLabel.text = subPost.timePosted
        cell.PostTitle.text = "• \(subPost.postTitle)"
        if let validURl = URL(string: subPost.URL){
            //cell.WebView.loadRequest(URLRequest(url: validURl))
            cell.WebView.load(URLRequest(url: validURl))
            
        }
        
        return cell
    }
    
    func getPosts(){
        allFollowed = []
        allPosts = []
        ref.child("users").child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? NSDictionary{
                if let allFollowing = value["following"] as? [String: Any]{
                    for f in allFollowing{
                        self.allFollowed.append(f.key)
                    }
                    self.ref.child("VideoPosts").observeSingleEvent(of: .value, with: { (snapshot) in
                        if let value = snapshot.value as? NSDictionary{
                            for f in self.allFollowed{
                                if let following = value[f] as? [String : Any]{
                                    let date = following["Date"] as? String
                                    let postedBy = following["Posted By"] as? String
                                    let title = following["Title"] as? String
                                    let url = following ["URL"] as? String
                                    
                                    self.allPosts.append(YoutubePost.init(URL: url!, postedBy: postedBy!, postTitle: title!, timePosted: date!))
                                }
                            }
                            DispatchQueue.main.async {
                                self.URLLabel.text = ""
                                self.VideoTitleLabel.text = ""
                                self.newsFeedTableView.reloadData()
                            }
                        }
                    })
                    
                }
            }
        })
    }
    
    @IBAction func UploadPressed(_ sender: Any) {
        if !(VideoTitleLabel.text?.isEmpty)! && !(URLLabel.text?.isEmpty)! {
            if let validURl = URL(string: URLLabel.text!){
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
                let time = dateFormatter.string(from: date)
                
                let tempPost = YoutubePost.init(URL: validURl.description, postedBy: userProfile.summonerName, postTitle: VideoTitleLabel.text!, timePosted: time)
                let sendMe = ["Date":tempPost.timePosted, "Posted By" : tempPost.postedBy, "Title" : tempPost.postTitle, "URL" : tempPost.URL]
                ref.child("VideoPosts").child((Auth.auth().currentUser?.uid)!).setValue(sendMe)
                
                getPosts()
            }
            
            dismissPostView(UIButton())
            postedView.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.postedView.isHidden = true
            }
        }
    }
    
    // Function to show the postview with animation
    @IBAction func postVideo(_ sender: UIButton) {
        postViewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.3) {
            self.dismissPostViewButton.isHidden = false
        }
    }
    
    // Function to "hide" the post view if user taps on the screen
    @IBAction func dismissPostView(_ sender: UIButton) {
        let height = keyboardHeight + 300
        postViewBottomConstraint.constant = height
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
            self.view.endEditing(true)
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.3) {
            self.dismissPostViewButton.isHidden = true
        }
        
    }
    
    // MARK: @objc KeyboardWillShow
    // Function to update bottom constraint of the bottom view so the user can see the textView when the keyboard shows up. Dismiss the keyboard the return the constraint back to normal
    @objc func keyBoardWillShow(sender: Notification) {
        if let userInfo = sender.userInfo as NSDictionary? {
            if let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as? NSValue, let duration = userInfo.value(forKey: UIKeyboardAnimationDurationUserInfoKey) as? NSNumber {
                self.keyboardHeight = keyboardFrame.cgRectValue.height
                self.keyboardDuration = TimeInterval(truncating: duration)
                if self.postViewBottomConstraint.constant == 0 {
                    self.postViewBottomConstraint.constant -= keyboardHeight
                    UIView.animate(withDuration: self.keyboardDuration) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    // MARK: Conform UITextField method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        if textField.tag == 1 {
            UploadPressed(UIButton())
        }
        
        return true
    }
}
