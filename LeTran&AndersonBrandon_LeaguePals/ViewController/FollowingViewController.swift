//
//  FollowingViewController.swift
//  
//
//  Created by Brandon Anderson on 7/26/18.
//

import UIKit
import FirebaseAuth
import Firebase

class FollowingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var followingTableView: UITableView!
    
    var ref : DatabaseReference!
    
    var activeUser : User!
    var currentProfile : SummonerProfile!
    var apiKey = ""
    var chosenIndex = 0
    var followingUID : [String] = []
    var following : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return following.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = followingTableView.dequeueReusableCell(withIdentifier: "followingCell", for: indexPath) as? FollowingTableViewCell else {
            return followingTableView.dequeueReusableCell(withIdentifier: "followingCell", for: indexPath)
        }
        
        cell.NameLabel.text = following[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        chosenIndex = indexPath.row
        self.followingTableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "FollowerProfile", sender: self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? OtherPlayerProfileViewController {
            destination.otherPlayerUID = followingUID[chosenIndex]
            
            destination.currentUser = activeUser
            destination.currentProfile = currentProfile
            destination.apiKey = apiKey
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetAllInfo()
        
    }
    
    func GetAllInfo(){
        
        ref = Database.database().reference()
        followingUID = []
        following = []
        followingTableView.reloadData()
        ref.child("users").child(activeUser.uid).child("following").observeSingleEvent(of : .value, with: { (snapshot) in
            
            if let value = snapshot.value as? [String: String] {
                for v in value{
                    if v.key != "Admin" && v.key != self.activeUser.uid{
                        self.followingUID.append(v.key)
                        print(v.key)
                    }
                    
                }
                for f in self.followingUID{
                    self.ref.child("users").child(f).observeSingleEvent(of : .value, with: { (snapshot) in
                        
                        if let value = snapshot.value as? [String: Any] {
                            
                            let followingName = value["summonerName"] as? String
                            self.following.append(followingName!)
                            DispatchQueue.main.async {
                                print(self.following.count)
                                self.followingTableView.reloadData()
                            }
                        }
                    })
                }
                
            }
        })
    }
    
    @IBAction func GoBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
