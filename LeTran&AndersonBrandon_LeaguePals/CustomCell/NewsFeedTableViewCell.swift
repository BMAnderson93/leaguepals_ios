//
//  NewsFeedTableViewCell.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/25/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit
import WebKit

class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var PostTitle: UILabel!
    @IBOutlet weak var DatePostedLabel: UILabel!
    @IBOutlet weak var AuthorLabel: UILabel!
    @IBOutlet weak var WebView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
