//
//  DetailReportCell.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/25/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit

class DetailReportCell: UITableViewCell {

    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var detailedReasonTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
