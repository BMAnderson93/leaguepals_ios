//
//  FollowingTableViewCell.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/26/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit

class FollowingTableViewCell: UITableViewCell {

    @IBOutlet weak var NameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
