//
//  ManageGroupTableViewCell.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/19/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit

class ManageGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var RankViewImageView: UIImageView!
    @IBOutlet weak var RoleViewImageView: UIImageView!
    @IBOutlet weak var PlayerName: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
