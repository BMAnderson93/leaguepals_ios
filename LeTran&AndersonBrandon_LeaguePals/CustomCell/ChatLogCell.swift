//
//  ChatLogCell.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/17/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit

class ChatLogCell: UITableViewCell {

    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
