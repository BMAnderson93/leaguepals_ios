//
//  LookForGroupCell.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/15/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import UIKit

class LookForGroupCell: UITableViewCell {

    @IBOutlet weak var TopButton: UIButton!
    @IBOutlet weak var JungButton: UIButton!
    @IBOutlet weak var MidButton: UIButton!
    @IBOutlet weak var ADCButton: UIButton!
    @IBOutlet weak var SupButton: UIButton!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var posterNameLabel: UIButton!
    @IBOutlet weak var gameModeLabel: UILabel!
    @IBOutlet var roleViewCollection: [UIView]!
    @IBOutlet weak var micImageView: UIImageView!
    @IBOutlet weak var rankRequirementLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
