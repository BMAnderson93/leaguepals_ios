//
//  ProfileViewExtension.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/14/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import UIKit

extension ProfileViewController{
    
    //This will download the users profile image and set it as their icon
    func AssignProfileImage(url:String){
        
        if let url = URL(string: url){
            do {
                //Creates a UIImage from the URL created
                let data = try Data(contentsOf: url)
                self.summonerProfileIcon_ImageView.image = UIImage(data: data)!
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func GetRating(){
        var sum = 0
        var rating = 0
        for r in userProfile.ratings{
            sum += r
            print(sum)
            print(r)
        }
        rating = sum / userProfile.ratings.count
        
        for i in ratingStars{
            if i.tag <= rating{
                i.image = #imageLiteral(resourceName: "Star")
            }
            else{
                i.image = #imageLiteral(resourceName: "EmptyStar")
            }
        }
    }
    
    //This gets all the rank stats needed for the users profile page
    func GetRankStats(summonerID: Int){
        
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        
        //Buts the url we need to get the rank data, the API will change daily
        if let validURL = URL(string: "https://na1.api.riotgames.com/lol/league/v3/positions/by-summoner/\(self.userProfile.summonerID)?api_key=\(self.apiKey)"){
            
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String: AnyObject]] {
                        //Parse Data
                        for i in json {
                        //Drills into the data, children, data to get the information I need from the json
                        self.summonerRank = (i["tier"] as? String)!
                        self.summonerWins = (i["wins"] as? Int)!
                        self.summonerLosses = (i["losses"] as? Int)!
                        self.summonerCurrentLP = (i["leaguePoints"] as? Int)!
                            
                            
                        print(self.summonerRank)
                        print(self.summonerWins)
                        print(self.summonerLosses)
                        print(self.summonerCurrentLP)
                        }
                        
                    }
                    
                    //Updates all the information to the profile
                    DispatchQueue.main.async {
                        
                        self.summonerWinLoss.text = "\(self.summonerWins) / \(self.summonerLosses)"
                        self.summonerLP.text = self.summonerCurrentLP.description
                        self.summonerName_Label.text = self.userProfile.summonerName
                        
                        //Checks what rank the user is and displays the correct badge
                        switch self.summonerRank{
                        case "BRONZE":
                            self.summonerRankImage.image = #imageLiteral(resourceName: "BronzeBadge")
                        case "SILVER":
                            self.summonerRankImage.image = #imageLiteral(resourceName: "SilverBadge")
                        case "GOLD":
                            self.summonerRankImage.image = #imageLiteral(resourceName: "GoldBadge")
                        case "PLATINUM":
                            self.summonerRankImage.image = #imageLiteral(resourceName: "PlatinumBadge")
                        case "DIAMOND":
                            self.summonerRankImage.image = #imageLiteral(resourceName: "DiamondBadge")
                        case "MASTER":
                            self.summonerRankImage.image = #imageLiteral(resourceName: "MasterBadge")
                        case "CHALLENGER":
                            self.summonerRankImage.image = #imageLiteral(resourceName: "ChallengerBadge")
                            
                        default:
                            print("MEH")
                        }
                    }
                    
                }
                catch {
                    print(error.localizedDescription)
                }
            })
            task.resume()
            
        }
        
    }
    
   
}
