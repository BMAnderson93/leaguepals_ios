//
//  SignUpExtension.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/14/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

extension SignUpViewController  {
    
   
    func CreateProfile(){
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        let newString = summonerNameTxtField.text?.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
        // Creates the url neede to get the summoner profiles information, api changes daily
        print("https://na1.api.riotgames.com/lol/summoner/v3/summoners/by-name/\(newString!)?api_key=\(apiKey)")
        if let validURL = URL(string: "https://na1.api.riotgames.com/lol/summoner/v3/summoners/by-name/\(newString!)?api_key=\(apiKey)"){
            
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                print(validURL.absoluteString)
                //Bail Out on error
                if opt_error != nil {print(opt_error.debugDescription)
                    return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                        //Parse Data
                        //Drills into the data, children, data to get the information I need from the json
                       let summonerName = json["name"] as? String
                       let summonerID = json["id"] as? Int
                       var summonerIconID = 3507
                        if let summonerIconIDCheck = json["profileIconId"] as? Int {
                            print(summonerIconIDCheck)
                            summonerIconID = summonerIconIDCheck
                        }
                       
                       let summonerLevel = json["summonerLevel"] as? Int
                        
                        self.createdProfile = SummonerProfile.init(appID: self.createdUser.uid, summonerName: summonerName!, summonerID: summonerID!, summonerIconId: summonerIconID, summonerLevel: summonerLevel!, hasPost: false, hasJoinedGroup: false, isBanned: false, following: [], ratings: [5])
                        
                        //Saves all the users information to a array
                      
                            
                        let profileInfo = ["summonerName" : self.createdProfile.summonerName, "summonerId" : self.createdProfile.summonerID, "summonerLevel" : self.createdProfile.summonerLevel, "summonerIconId" : self.createdProfile.summonerIconID, "hasPost" : false, "isAdmin" : false, "hasGroup" : false, "isBanned" : false, "ratings" : [5]] as [String : Any]
                        
                        //Adds the new user and their information to the database
                        self.ref.child("users").child(self.createdUser.uid).setValue(profileInfo)
                        self.ref.child("users").child(self.createdUser.uid).child("following").child("Admin").setValue("Following")
                        self.ref.child("users").child(self.createdUser.uid).child("following").child(self.createdUser.uid).setValue("Following")
                        
                        
                        
                        print(self.createdProfile.summonerIconID)
                    }
                    
                    DispatchQueue.main.async {
                         self.performSegue(withIdentifier: "SignUpToProfile", sender: self)
                    }
                     
                }
                catch {
                    print(error.localizedDescription)
                }
            })
            task.resume()
            
        }
    }
}
