//
//  SignUpExtension.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Brandon Anderson on 7/14/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import Firebase
extension ViewController  {
    
    
    func CreateProfile(){
        
        //Goes into the database and pulls down all the current users profile information then creates that profile
        self.ref?.child("users").child(currentUser.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? NSDictionary{
                
                var hasJoinGroup = false
                var allFollowing : [String] = []
                let summonerName = (value["summonerName"] as? String)!
                let summonerID = (value["summonerId"] as? Int)!
                let summonerLevel = (value["summonerLevel"] as? Int)!
                let summonerIconID = (value["summonerIconId"] as? Int)!
                let hasPost = (value["hasPost"] as? Bool)!
                if (value["hasGroup"] as? Bool) != nil {
                    hasJoinGroup =  (value["hasGroup"] as? Bool)!
                }
                if let following = value["following"] as? [String]{
                    allFollowing = following
                }
                let isBanned = (value["isBanned"] as? Bool)!
                let rating = (value["ratings"] as? [Int])!
             
                
                guard let isAdmin = value["isAdmin"] as? Bool else {
                    print("well I don't know what else to say")
                    return
                }
                
                self.userProfile = SummonerProfile.init(appID: self.currentUser.uid, summonerName: summonerName, summonerID: summonerID, summonerIconId: summonerIconID, summonerLevel: summonerLevel,hasPost : hasPost, isAdmin: isAdmin, hasJoinedGroup: hasJoinGroup, isBanned: isBanned, following: allFollowing, ratings: rating)
                
                DispatchQueue.main.async {
                    // Starts the segue to the profile view
                    if self.userProfile.isBanned == true {
                        print("This guy is banned, dont log in.")
                        self.loginStatusLabel.text = "This account has been banned. Please log in with another account"
                        self.loginStatusLabel.isHidden = false
                    } else {
                        self.performSegue(withIdentifier: "LoggedIn", sender: self)
                    }
                }
            }
            
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
    }
}
