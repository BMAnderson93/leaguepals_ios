//
//  OtherPlayer_Ext.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/15/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import UIKit

extension OtherPlayerProfileViewController {
    func GetRating(){
        var sum = 0
        var rating = 0
        
        for r in otherProfile.ratings{
            sum += r
        }
        rating = Int(round(Double(sum / otherProfile.ratings.count)))
        
        for i in ratingStars{
            if i.tag <= rating{
                i.image = #imageLiteral(resourceName: "Star")
            }
            else{
                i.image = #imageLiteral(resourceName: "EmptyStar")
            }
        }
    }
    
    func CreateProfile(){
        
        //Goes into the database and pulls down all the current users profile information then creates that profile
        self.ref.child("users").child(otherPlayerUID).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            if let value = snapshot.value as? NSDictionary{
                let summonerName = (value["summonerName"] as? String)!
                let summonerID = (value["summonerId"] as? Int)!
                let summonerLevel = (value["summonerLevel"] as? Int)!
                let summonerIconID = (value["summonerIconId"] as? Int)!
                let hasPost = (value["hasPost"] as? Bool)!
                let hasGroup = (value["hasGroup"] as? Bool)!
                
                var allFollowing : [String] = []
                if let following = value["following"] as? [String]{
                    allFollowing = following
                }
                let isBanned = (value["isBanned"] as? Bool)!
                let rating = (value["ratings"] as? [Int])!
                
              
                
                self.otherProfile = SummonerProfile.init(appID: self.otherPlayerUID, summonerName: summonerName, summonerID: summonerID, summonerIconId: summonerIconID, summonerLevel: summonerLevel, hasPost: hasPost, hasJoinedGroup: hasGroup, isBanned: isBanned, following: allFollowing, ratings: rating)
                
                print(summonerName)
                DispatchQueue.main.async {
                    //Starts the segue to the profile view
                    // Do any additional setup after loading the view.
                    self.profileIconID = self.otherProfile.summonerIconID
                    //This will build the api string we need for the profile icon of the user
                    let profileIconURL = "http://ddragon.leagueoflegends.com/cdn/8.13.1/img/profileicon/\(self.profileIconID).png"
                    self.AssignProfileImage(url: profileIconURL)
                    self.GetRankStats(summonerID: self.otherProfile.summonerID)
                    self.GetRating()
                    
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
    }
    
    //This will download the users profile image and set it as their icon
    func AssignProfileImage(url:String){
        
        if let url = URL(string: url){
            do {
                //Creates a UIImage from the URL created
                let data = try Data(contentsOf: url)
                self.summonerProfileIconImageView.image = UIImage(data: data)!
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    
    //This gets all the rank stats needed for the users profile page
    func GetRankStats(summonerID: Int){
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        
        //Buts the url we need to get the rank data, the API will change daily
        if let validURL = URL(string: "https://na1.api.riotgames.com/lol/league/v3/positions/by-summoner/\(self.otherProfile.summonerID)?api_key=\(self.apiKey)"){
            
            let task = session.dataTask(with: validURL, completionHandler: { (opt_data, opt_response, opt_error) in
                
                //Bail Out on error
                if opt_error != nil { return }
                
                //Check the response, statusCode, and data
                guard let response = opt_response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = opt_data
                    else { return }
                
                do {
                    //De-Serialize data object
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String: AnyObject]] {
                        //Parse Data
                        for i in json {
                            //Drills into the data, children, data to get the information I need from the json
                            self.summonerRank = (i["tier"] as? String)!
                            self.summonerWins = (i["wins"] as? Int)!
                            self.summonerLosses = (i["losses"] as? Int)!
                            self.summonerCurrentLP = (i["leaguePoints"] as? Int)!
                            
                            
                            print(self.summonerRank)
                            print(self.summonerWins)
                            print(self.summonerLosses)
                            print(self.summonerCurrentLP)
                        }
                        
                    }
                    
                    //Updates all the information to the profile
                    DispatchQueue.main.async {
                        
                        self.summonerWinLossLabel.text = "\(self.summonerWins) / \(self.summonerLosses)"
                        self.summonerLPLabel.text = self.summonerCurrentLP.description
                        self.summonerNameLabel.text = self.otherProfile.summonerName
                        
                        //Checks what rank the user is and displays the correct badge
                        switch self.summonerRank{
                        case "BRONZE":
                            self.summonerRankImageView.image = #imageLiteral(resourceName: "BronzeBadge")
                        case "SILVER":
                            self.summonerRankImageView.image = #imageLiteral(resourceName: "SilverBadge")
                        case "GOLD":
                            self.summonerRankImageView.image = #imageLiteral(resourceName: "GoldBadge")
                        case "PLATINUM":
                            self.summonerRankImageView.image = #imageLiteral(resourceName: "PlatinumBadge")
                        case "DIAMOND":
                            self.summonerRankImageView.image = #imageLiteral(resourceName: "DiamondBadge")
                        case "MASTER":
                            self.summonerRankImageView.image = #imageLiteral(resourceName: "MasterBadge")
                        case "CHALLENGER":
                            self.summonerRankImageView.image = #imageLiteral(resourceName: "ChallengerBadge")
                            
                        default:
                            print("MEH")
                        }
                    }
                    
                }
                catch {
                    print(error.localizedDescription)
                }
            })
            task.resume()
            
        }
        
    }
}
