//
//  UIColor_Ext.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/16/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import UIKit

// Create extension for UIColor to get color from hex code
extension UIColor {
    convenience init(hexCode: String) {
        let hex = hexCode.trimmingCharacters(in: CharacterSet.init(charactersIn: "#"))
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
