//
//  ManageReport_Ext.swift
//  LeTran&AndersonBrandon_LeaguePals
//
//  Created by Sunny Le on 7/24/18.
//  Copyright © 2018 Tran Le. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension ManageReportViewController {
    func getReportedUsers() {
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            let appId = snapshot.key
            print(appId)
            if let value = snapshot.value as? [String: Any] {
                guard let name = value["summonerName"] as? String,let hasGroup = value["hasGroup"] as? Bool, let level = value["summonerLevel"] as? Int, let summonerId = value["summonerId"] as? Int, let iconId = value["summonerIconId"] as? Int, let hasPost = value["hasPost"] as? Bool, let isAdmin = value["isAdmin"] as? Bool, let report = value["reportHistory"] as? [String: Any]
                    else {
                    print("Hell yeah, hell to go")
                    return
                }
                var allFollowing : [String] = []
                if let following = value["following"] as? [String] {
                    allFollowing = following
                }
                
                let isBanned = (value["isBanned"] as? Bool)!
                let rating = (value["ratings"] as? [Int])!
                print("hello me")
                if report.count > 0 {
                    let reportPerson = SummonerProfile(appID: appId, summonerName: name, summonerID: summonerId, summonerIconId: iconId, summonerLevel: level, hasPost: hasPost, isAdmin: isAdmin, hasJoinedGroup: hasGroup, isBanned: isBanned, following: allFollowing, ratings: rating)
                    if isBanned == false {
                        self.reportedUser.append(reportPerson)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.manageReportTableView.reloadData()
            }
        }, withCancel: nil)
    }
}
