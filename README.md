# LEAGUE PALS
##### Tran Le & Brandon Anderson
---
###### League Pals is a League of Legends application created to solve that crucial issue of finding that extra man to fill out your roster. We all know the struggle of trying to get 5 people together to enjoy a game, and having to que up to find a random team mate. League Pals helps to solve this issue by allowing users to find a perfect fit for their roster. So whether you want to compete competitively, or just want to find a new friend for some ARAM, League Pals is the app for you.

---

1. **Available Features**
- Log in / sign up for new account with email and password.
- Follow a user.
- Send private message.
- View game stats.
- View shared videos.
- Manage teammates.

2. **Hardware considerations and requirements**
- No information.

3. **Login requirements for testing ###
- email - test@gmail.com - Admin
- email - test2@gmail.com - Banned
- email - test3@gmail.com   - Banned
- email - test4@gmail.com
- password - password
  
  Summoner name you could use incase you dont have one when signing up - Doublelift
  
  Git Repository Link - https://bitbucket.org/n1nj4xh3ro/1807_letran-andersonbrandon/src/master/
  
  Final Feature List - https://trello.com/b/LPx5MZ2U/1807-sunny-le-brandon-anderson
